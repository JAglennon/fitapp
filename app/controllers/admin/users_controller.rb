module Admin
  class UsersController < ApplicationController
  layout 'admin_application'
   def index
    @users = User.where(activated: true).paginate(page: params[:page])
   end
   
  def destroy
    if current_user && current_user.admin?
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    end
    redirect_to admin_users_path
  end
      
  end
end
